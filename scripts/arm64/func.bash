# The file scripts/common.bash has to be the only file sourcing this
# arch helper file
source config.mak

ARCH_CMD=arch_cmd_arm64

extract_measurement()
{
    sed -n 's/^MEASUREMENT: *\(.*\)$/\1/p' "$1"
}

function arch_cmd_arm64()
{
    local cmd=$1
    local testname=$2
    local groups=$3
    local smp=$4
    local kernel=$5
    local opts=$6
    local arch=$7
    local check=$8
    local accel=$9
    local timeout=${10}

    "$@"
    ret=$?
    if [ $ret -ne 0 ] || [ "$cmd" != run_task ] ||
            ! find_word measurement "$groups"; then
        return $ret
    fi

    # For report_measurement tests, run twice and compare measurement.

    # run_task initializes RUNTIME_log_file. That's too late.
    local logf="${unittest_log_dir}/${testname}.log"
    first_measured=$(extract_measurement "$logf")
    if [ -z "$first_measured" ]; then
        print_result "FAIL" $testname "" "measurement not found"
        set +x
        return 1
    fi

    echo -n > "$logf"
    "$@"
    ret=$?
    if [ $ret -ne 0 ]; then
        return $ret
    fi

    last_measured=$(extract_measurement "$logf")
    if [ "$first_measured" != "$last_measured" ]; then
        print_result "FAIL" $testname "" \
            "inconsistent measurement '$first_measured' != '$last_measured'"
        return 1
    fi
    print_result "PASS" "$testname" "measured '$first_measured'"
    return 0
}
